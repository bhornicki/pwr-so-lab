#!/bin/bash -ue

DIR=${1}

# Check if DIR is a directory
if [[ ! -d ${DIR} ]]; then
	exit 2
fi

# unify relativepaths to absolute type - it's a workaround, but it works.
DIR=$(realpath ${DIR})

for ITEM in $(ls ${DIR}); do
    # *.bak file
    if [[ -f ${DIR}/${ITEM} && ${ITEM##*.} == "bak" ]]; then
        #user & other cannot read
        $(chmod uo-w ${DIR}/${ITEM})
    
    # *.bak folder
    elif [[ -d ${DIR}/${ITEM} && ${ITEM##*.} == "bak" ]]; then
        #others only can visit - assume they always can, not only if they previously were allowed!
        $(chmod o+r ${DIR}/${ITEM})
        $(chmod ug-r ${DIR}/${ITEM})
    
    # *.tmp folder
    elif [[ -d ${DIR}/${ITEM} && ${ITEM##*.} == "tmp" ]]; then
        #everyone can create file
        $(chmod a+w ${DIR}/${ITEM})
    
    # *.txt file
    elif [[ -f ${DIR}/${ITEM} && ${ITEM##*.} == "txt" ]]; then
        #user read, group write, others exec
        $(chmod u=r ${DIR}/${ITEM})
        $(chmod g=w ${DIR}/${ITEM})
        $(chmod o=x ${DIR}/${ITEM})
    
    # *.exe file
    elif [[ -f ${DIR}/${ITEM} && ${ITEM##*.} == "exe" ]]; then
        #all exec, as user
        $(chmod a+x ${DIR}/${ITEM})
        #sticky stuff
        $(chmod u+s ${DIR}/${ITEM})
    fi
done
