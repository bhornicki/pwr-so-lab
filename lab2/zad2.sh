#!/bin/bash -ue

DIR=${1}
OUTPUT_FILE=${2}

# Check if DIR is a directory
if [[ ! -d ${DIR} ]]; then
	exit 1
# Check if OUTPUT_FILE does not exist, make it
elif [[ ! -e ${OUTPUT_FILE} ]]; then
    touch ${OUTPUT_FILE}
# Check if OUTPUT_FILE is not a regular file
elif [[ ! -f ${OUTPUT_FILE} ]]; then
    exit 1
fi

# unify relativepaths to absolute type - it's a workaround, but it works.
DIR=$(realpath ${DIR})
OUTPUT_FILE=$(realpath ${OUTPUT_FILE})

# Voodoo magic here: check if symlink is broken and report its name to a file
for ITEM in $(ls ${DIR}); do
    if [[ -L ${DIR}/${ITEM} && ! -e ${DIR}/${ITEM} ]]; then
        TIMESTAMP=$(date --iso-8601)
        echo "[${TIMESTAMP}] ${ITEM}" >> ${OUTPUT_FILE}
        rm ${DIR}/${ITEM}
    fi
done