#!/bin/bash -ue

SRC=${1}
DEST=${2}

# Check if SRC is a directory
if [[ ! -d ${SRC} ]]; then
	exit 1
# Check if DEST does not exist and make it
elif [[ ! -e ${DEST} ]]; then
    mkdir ${DEST}
# Check if DEST is not a dir
elif [[ ! -d ${DEST} ]]; then
    exit 1
fi

# unify relativepaths to absolute type - it's a workaround, but it works.
SRC=$(realpath ${SRC})
DEST=$(realpath ${DEST})

for ITEM in $(ls ${SRC}); do
	if [[ -L ${SRC}/${ITEM} ]]; then
        echo "${ITEM} to symlink"
    elif [[ -d ${SRC}/${ITEM} ]]; then
        echo "${ITEM} to katalog"
        ln -s ${SRC}/${ITEM} ${DEST}/${ITEM}_ln
    elif [[ -f ${SRC}/${ITEM} ]]; then
        echo "${ITEM} to plik"
        NAME=${ITEM%.*}
        EXT=${ITEM##*.}
        #File with no extension?
        if [[ ${NAME} == ${EXT} ]]; then
            RES=${DEST}/${ITEM%.*}_ln
        else
            RES=${DEST}/${ITEM%.*}_ln.${ITEM##*.}
        fi
        ln -s ${SRC}/${ITEM} ${RES}
    fi
done
