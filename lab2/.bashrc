# ~/.bashrc
# Davram BASHere?

_BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
_TIMESTAMP=$(date +"%Y.%m.%d_%H:%M")
PS1='\e[0;35m[\u@\h \W ${_TIMESTAMP} ${_BRANCH}]% \e[0;32m'
PATH=$PATH:.