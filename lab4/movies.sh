#!/bin/bash -eu

# + 1.0: shellcheck nie krzyczy. Unikamy "disable"
#probably caught everything

function print_help () {
    echo "This script allows to search over movies database"
    echo -e "-d DIRECTORY\n\tDirectory with files describing movies"
    echo -e "-a ACTOR\n\tSearch movies that this ACTOR played in"
    echo -e "-t QUERY\n\tSearch movies with given QUERY in title"
    echo -e "-y YEAR\n\tSearch movies released during or after given YEAR"
    echo -e "-f FILENAME\n\tSaves results to file (default: results.txt)"
    echo -e "-x\n\tPrints results in XML format"
    echo -e "-h\n\tPrints this help message"
}

function print_error () {
    echo -e "\e[31m\033[1m${*}\033[0m" >&2
}

function get_movies_list () {
    local -r MOVIES_DIR=${1}
    local -r MOVIES_LIST=$(cd "${MOVIES_DIR}" && realpath ./*)
    echo "${MOVIES_LIST}"
}

function query_title () {
    # Returns list of movies from ${1} with ${2} in title slot
    local -r MOVIES_LIST=${1}
    local -r QUERY=${2}

    local RESULTS_LIST=()
    for MOVIE_FILE in ${MOVIES_LIST}; do
        if grep "| Title" "${MOVIE_FILE}" | grep -q "${QUERY}"; then
            RESULTS_LIST+=( "${MOVIE_FILE}" )
        fi
    done
    echo "${RESULTS_LIST[@]:-}"
}

function query_actor () {
    # Returns list of movies from ${1} with ${2} in actor slot
    local -r MOVIES_LIST=${1}
    local -r QUERY=${2}

    local RESULTS_LIST=()
    for MOVIE_FILE in ${MOVIES_LIST}; do
        if grep "| Actors" "${MOVIE_FILE}" | grep -q "${QUERY}"; then
            RESULTS_LIST+=( "${MOVIE_FILE}" )
        fi
    done
    echo "${RESULTS_LIST[@]:-}"
}

function query_year () {
    # Returns list of movies from ${1} with at least ${2} in year slot
    local -r MOVIES_LIST=${1}
    local -r QUERY=${2}

    local RESULTS_LIST=()
    for MOVIE_FILE in ${MOVIES_LIST}; do
        if [[ $(grep "| Year" "${MOVIE_FILE}" | cut -d: -f2) -ge ${QUERY} ]]; then
            RESULTS_LIST+=( "${MOVIE_FILE}" )
        fi
    done
    echo "${RESULTS_LIST[@]:-}"
}

function query_plot () {
    # Returns list of movies from ${1} with matching ${2} regex in plot slot
    local -r MOVIES_LIST=${1}
    local -r QUERY=${2}

    local RESULTS_LIST=()
    if ${SEARCHING_PLOT_CASE_INSENSITIVE:-false}; then
        for MOVIE_FILE in ${MOVIES_LIST}; do
            if grep "| Plot" "${MOVIE_FILE}" | grep -qiP "${QUERY}"; then
                RESULTS_LIST+=( "${MOVIE_FILE}" )
            fi
        done
    else
        for MOVIE_FILE in ${MOVIES_LIST}; do
            if grep "| Plot" "${MOVIE_FILE}" | grep -qP "${QUERY}"; then
                RESULTS_LIST+=( "${MOVIE_FILE}" )
            fi
        done
    fi
    echo "${RESULTS_LIST[@]:-}"
}

function print_xml_format () {
    local -r FILENAME=${1}

    local TEMP
    TEMP=$(cat "${FILENAME}")
    # +1.0:  Dokończ funkcję “print_xml_format”
    # https://youtu.be/m1yWUWhamj4

    # change "| TEXT: VALUE" to "<TEXT>VALUE</TEXT>" tag notation"
    TEMP=$(echo "${TEMP}" | sed -r 's/\| ([A-Za-z]+): ?(.*)/\t<\1>\2<\/\1>/')
    
    # replace first line of equals signs
    TEMP=$(echo "${TEMP}" | sed '0,/===*/s//<movie>/')

    # replace the last line of equals with </movie>
    TEMP=$(echo "${TEMP}" | sed '$s/===*/<\/movie>/')

    echo "${TEMP}"
}

function print_movies () {
    local -r MOVIES_LIST=${1}
    local -r OUTPUT_FORMAT=${2}

    for MOVIE_FILE in ${MOVIES_LIST}; do
        if [[ "${OUTPUT_FORMAT}" == "xml" ]]; then
            print_xml_format "${MOVIE_FILE}"
        else
            cat "${MOVIE_FILE}"
        fi
    done
}

ANY_ERRORS=false

while getopts :hd:t:a:xf:y:R:i OPT; do
  case ${OPT} in
    h)
        print_help
        exit 0
        ;;
    d)
        MOVIES_DIR=${OPTARG}
        ;;
    t)
        SEARCHING_TITLE=true
        QUERY_TITLE=${OPTARG}
        ;;
    f)
        FILE_4_SAVING_RESULTS=${OPTARG}
        ;;
    a)
        SEARCHING_ACTOR=true
        QUERY_ACTOR=${OPTARG}
        ;;
    x)
        OUTPUT_FORMAT="xml"
        ;;
    y)
        # + 1.0: Dodaj opcję -y ROK: wyszuka wszystkie filmy nowsze niż ROK. Pamiętaj o dodaniu opisu do -h
        SEARCHING_YEAR=true
        QUERY_YEAR=${OPTARG}
        ;;
    R)
        # + 1.0: Dodaj wyszukiwanie po polu z fabułą, za pomocą wyrażenia regularnego. Np. -R 'Cap.*Amer'
        SEARCHING_PLOT=true
        QUERY_PLOT=${OPTARG}
        ;;
    i)
        # ... + 1.0: Dodaj wyszukiwanie, jeżeli dodatkowo podamy parametr '-i' to ignoruje wielkość liter
        SEARCHING_PLOT_CASE_INSENSITIVE=true
        ;;
        
    \?)
        print_error "ERROR: Invalid option: -${OPTARG}"
        ANY_ERRORS=true
        export ANY_ERRORS
        exit 1
        ;;
  esac
done

# + 0.5: Dodaj sprawdzenie, czy na pewno wykorzystano opcję '-d'
if [[ -z ${MOVIES_DIR+x} ]]; then
    echo "ERROR: No directory provided"
    exit 1
fi

# ... + 0.5: Dodaj sprawdzenie, czy jest to katalog
if [[ ! -d ${MOVIES_DIR} ]]; then
    echo "ERROR: Provided directory does not identify itself as a directory"
    exit 1
fi

MOVIES_LIST=$(get_movies_list "${MOVIES_DIR}")

if ${SEARCHING_TITLE:-false}; then
    MOVIES_LIST=$(query_title "${MOVIES_LIST}" "${QUERY_TITLE}")
fi

if ${SEARCHING_ACTOR:-false}; then
    MOVIES_LIST=$(query_actor "${MOVIES_LIST}" "${QUERY_ACTOR}")
fi

if ${SEARCHING_YEAR:-false}; then
    MOVIES_LIST=$(query_year "${MOVIES_LIST}" "${QUERY_YEAR}")
fi

if ${SEARCHING_PLOT:-false}; then
    MOVIES_LIST=$(query_plot "${MOVIES_LIST}" "${QUERY_PLOT}")
fi

if [[ "${#MOVIES_LIST}" -lt 1 ]]; then
    echo "Found 0 movies :-("
    exit 0
fi

if [[ "${FILE_4_SAVING_RESULTS:-}" == "" ]]; then
    print_movies "${MOVIES_LIST}" "${OUTPUT_FORMAT:-raw}"
else
    # + 0.5: Dotyczy opcji ‘-f’: jeżeli plik podany przez użytkownika nie posiada rozszerzenia '.txt' dodaj je
    if [[ ${FILE_4_SAVING_RESULTS} != *".txt" ]]; then
        FILE_4_SAVING_RESULTS+=".txt"
    fi
    
    print_movies "${MOVIES_LIST}" "${OUTPUT_FORMAT:-raw}" | tee "${FILE_4_SAVING_RESULTS}"
fi
