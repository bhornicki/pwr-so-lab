#!/bin/bash

#+0.5 – napisać skrypt, który pobiera 3 argumenty: 
# SOURCE_DIR, RM_LIST, TARGET_DIR o wartościach domyślnych: 
# lab_uno, lab_uno /2remove, bakap
SOURCE_DIR=${1:-"lab_uno"}
RM_LIST=${2:-"lab_uno/2remove"}
TARGET_DIR=${3:-"bakap"}

#+0.5 – jeżeli TARGET_DIR nie istnieje, to go tworzymy
if [[ ! -d ${TARGET_DIR} ]]; then
	mkdir ${TARGET_DIR}
fi

#+1.0 – iterujemy się po zawartości pliku RM_LIST 
# i tylko jeżeli plik o takiej nazwie występuje w katalogu SOURCE_DIR, to go usuwamy
for ITEM in $(cat ${RM_LIST}); do
	if [[ -f ${SOURCE_DIR}/${ITEM} ]]; then
		rm ${SOURCE_DIR}/${ITEM}
	fi
done

#+0.5 x2 – jeżeli jakiegoś pliku nie ma na liście, 
# ale jest plikiem regularny, to przenosimy go do TARGET_DIR
# ale jest katalogiem, to kopiujemy go do TARGET_DIR z zawartością
for ITEM in $(ls ${SOURCE_DIR}); do
	if [[ -f ${SOURCE_DIR}/${ITEM} ]]; then
		mv ${SOURCE_DIR}/${ITEM} ${TARGET_DIR}
	elif [[ -d ${SOURCE_DIR}/${ITEM} ]]; then
		cp -r ${SOURCE_DIR}/${ITEM} ${TARGET_DIR}
	fi
done

#+1.0  – jeżeli po zakończeniu operacji są jeszcze jakieś pliki w katalogu SOURCE_DIR to:
FILE_CNT=$(ls ${SOURCE_DIR} | wc -w)

#piszemy np. „jeszcze coś zostało” na konsolę oraz..
if [[ ${FILE_CNT} -gt 0 ]]; then
	echo "Więcej niż jeden plik to: Owca? Lama?"
	
	#..oraz jeżeli co najmniej 2 pliki, to wypisujemy: „zostały co najmniej 2 pliki”
	if [[ ${FILE_CNT} -ge 2 ]]; then
		echo "Zostały co najmniej 2 pliki"
	fi

	#..oraz jeżeli więcej niż 4, to wypisujemy: „zostało więcej niż 4 pliki” (UWAGA: 4, to też więcej niż 2)
	if [[ ${FILE_CNT} -gt 4 ]]; then
		echo "Zostało więcej niż 4 pliki"
	fi

	#..oraz jeżeli nie więcej, niż 4, ale co najmniej 2, to też coś piszemy
	if [[ ${FILE_CNT} -ge 2 && ${FILE_CNT} -le 4 ]]; then
		echo "Też coś piszemy (2<= <=4)"
	fi
else
	#Jeżeli nic nie zostało, to informujemy o tym słowami np. „tu był Kononowicz”
	echo "Ninja nie pozostawia żadnych śladów"
fi

#+0.5 – wszystkie pliki w katalogu TARGET_DIR muszą mieć odebrane prawa do edycji
# Tego już nie zmienisz, bo zostało wklepane do excela!
for ITEM in $(ls ${TARGET_DIR}); do
	if [[ -f ${TARGET_DIR}/${ITEM} ]]; then
		chmod a-w ${TARGET_DIR}/${ITEM}
	fi
done

#+0.5 – po wszystkich spakuj katalog TARGET_DIR i nazwij bakap_DATA.zip, 
# gdzie DATA to dzień uruchomienia skryptu w formacie RRRR-MM-DD
TIMESTAMP=$(date +"%Y-%m-%d")
zip -qr bakap_${TIMESTAMP}.zip ${TARGET_DIR}