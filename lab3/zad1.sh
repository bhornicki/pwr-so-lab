#!/bin/bash -ue

SRC=${1:-"access_log"}

# Znajdź w pliku access_log zapytania, które mają frazę ""denied"" w linku
echo "Znajdź w pliku access_log zapytania, które mają frazę denied w linku"
cat ${SRC} | grep -i "/denied"
echo

# Znajdź w pliku access_log zapytania typu POST
echo "Znajdź w pliku access_log zapytania typu POST"
cat ${SRC} | grep "\"POST "    #bash my way through the grep!
echo

# Znajdź w pliku access_log zapytania wysłane z IP: 64.242.88.10
echo "Znajdź w pliku access_log zapytania wysłane z IP: 64.242.88.10"
cat ${SRC} | grep "^64\.242\.88\.10 "
echo

# Znajdź w pliku access_log wszystkie zapytania NIEWYSŁANE z adresu IP tylko z FQDN
#Pułapki:
#3_343_lt_someone
#[Sun Mar  7 17:23:53 2004]
#64x242x88x10
echo "Znajdź w pliku access_log wszystkie zapytania NIEWYSŁANE z adresu IP tylko z FQDN"
cat ${SRC} \
    | grep -Pv "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} "   \
    | grep -P "^[a-zA-Z0-9_\-]+\.[a-zA-Z0-9_\-\.]+ "
echo

# Znajdź w pliku access_log unikalne zapytania typu DELETE
echo "Znajdź w pliku access_log unikalne zapytania typu DELETE"
   cat ${SRC} | grep "DELETE" | sort | uniq
echo

# Znajdź unikalnych 10 adresów IP w access_log
echo "Znajdź unikalnych 10 adresów IP w access_log"
cat ${SRC} \
    | grep -P "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} "   \
    | sort | uniq -w 16 #Dzięki za " - - ["!
echo