#!/bin/bash -ue

SRC=${1:-"yolo.csv"}

# Z pliku yolo.csv wypisz wszystkich, których id jest liczbą nieparzystą. Wyniki zapisz na standardowe wyjście błędów.
cat ${SRC} | sed 1d \
    | grep -P "^[0-9]*[13579]," \
    | cut -d',' -f1,2,3 \
    | sed 's&,& &g' \
    >&2
echo

# Z pliku yolo.csv wypisz każdego, kto jest wart dokładnie $2.99 lub $5.99 lub $9.99. Nie wazne czy milionów, czy miliardów (tylko nazwisko i wartość). Wyniki zapisz na standardowe wyjście błędów
cat ${SRC} | sed 1d \
    | cut -d',' -f2,3,7 \
    | grep -P "\\\$(2\.99|5\.99|9\.99)[a-zA-Z ]+" \
    | sed 's&,& &g' \
    >&2
echo

# Z pliku yolo.csv wypisz każdy numer IP, który w pierwszym i drugim oktecie ma po jednej cyfrze. Wyniki zapisz na standardowe wyjście błędów
cat ${SRC} | sed 1d \
    | cut -d',' -f6 \
    | grep -P "^[0-9]\.[0-9]\.[0-9]{1,3}\.[0-9]{1,3}" \
    >&2