#!/bin/bash -ue

# Zadanie dla chętnich: napisz jednolinijkowca, któy klonuje (robi echo) repozytoria z ludzie.csv, po SSH, do katalogow imie_nazwisko (malymi literami).
# Zwróccie uwagę, że niektóre repozytoria mają '.git' inne nie, to trzeba zunifikować! (dam za to +0.5)

#that CR was sneaky...
#Usage of ';' is legal in this task. Change my mind.
SRC=${1:-"ludzie.csv"}; LIST=$(cat ${SRC} | sed 1d | cut -d',' -f1,3 | sed 's|\(.*,\)|\L\1|' | sed 's& &_&' | sed 's|\r||' | sed -r 's|\/[a-zA-Z0-9\-]+$|&.git|g' | sed 's|https*://github.com/|git@github.com:|'); for STR in ${LIST}; do FOLDER=${STR%,*}; REPO=${STR#*,}; echo "git clone ${REPO} ${FOLDER}"; done