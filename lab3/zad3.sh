#!/bin/bash -ue

SRC=${1:-"groovies"}
FILES=$(ls ${SRC})

# We wszystkich plikach w katalogu ‘groovies’ zamień $HEADER$ na /temat/
for FILE in ${FILES}; do
    sed -i 's&\$HEADER\$&/temat/&g' ${SRC}/${FILE}
done

# We wszystkich plikach w katalogu ‘groovies’ po każdej linijce z 'class' dodać '  String marker = '/!@$%/''
# 'Po każdej linijce' rozumiem jako pod koniec każdej linijki zawierającej słowo 'class'
# Wstawiany fragment nie ma sensu, ale 'I do as the Szandała guides', ważne że działa! :D
for FILE in ${FILES}; do
    sed -ri "s|class (.*)$|&  String marker = '\/!@\\\$%\/'|g" ${SRC}/${FILE}
done

# We wszystkich plikach w katalogu ‘groovies’ usuń linijki zawierające frazę 'Help docs:'
for FILE in ${FILES}; do
    sed -i '/Help docs:/d' ${SRC}/${FILE}
done