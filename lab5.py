#!/usr/bin/env python3
'''Python game.'''
import random
import time
import sys
from enum import Enum

#TacTic Toe <insert toe in military glove here or something>

class Gamemode(Enum):
    '''Main menu game mode options'''
    PVP = 1
    PVC = 2
    CVP = 3
    CVC = 4
    EXIT = 0

#remap numeric keys
KEYS = {
    1: [2, 0],
    2: [2, 1],
    3: [2, 2],
    4: [1, 0],
    5: [1, 1],
    6: [1, 2],
    7: [0, 0],
    8: [0, 1],
    9: [0, 2],
}

GAME = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
PLAYER = 'X'
COMPUTER = 'D'


def display():
    '''displays gameboard'''
    # come on, that's so ulgy
    board = f"""\
+-+-+-+
|{GAME[0][0]}|{GAME[0][1]}|{GAME[0][2]}|
+-+-+-+
|{GAME[1][0]}|{GAME[1][1]}|{GAME[1][2]}|
+-+-+-+
|{GAME[2][0]}|{GAME[2][1]}|{GAME[2][2]}|
+-+-+-+
    """
    print(board)


def make_move(target=PLAYER):
    '''
    wait for player input, validate it and register move.
    Returns false when invalid value is provided or field is already occupied
    '''
    global GAME

    try:
        key = int(input()[0])
    except ValueError:
        return False

    if key > 9 or key < 0:
        return False

    if key == 0:
        # ah yes, I want to see the stacktrace <3
        menu()

    [i, j] = KEYS[key]
    if GAME[i][j] == ' ':
        GAME[i][j] = target
        return True
    return False


def find_winner():
    '''returns signature of game winner, otherwise None'''
    # Premature optimization: Let's make the code beautifuln't!

    # diagonals
    ret = GAME[1][1]
    if GAME[0][0] == GAME[1][1] == GAME[2][2] != ' ':
        return ret
    if GAME[0][2] == GAME[1][1] == GAME[2][0] != ' ':
        return ret

    # rows and cols
    for i in range(3):
        ret = GAME[i][i]
        if GAME[i][0] == GAME[i][1] == GAME[i][2] != ' ':
            return ret
        if GAME[0][i] == GAME[1][i] == GAME[2][i] != ' ':
            return ret

    return None


def find_third_in_row(free_list, target=PLAYER):
    '''
    returns [row,column] of a field, which will be the third (winning) element in a line.
    If no field is matched this way, returns None.
    '''
    global GAME
    # Eh, good enough
    for item in free_list:
        [i, j] = item
        i0 = (i + 1) % 3
        i1 = (i + 2) % 3
        j0 = (j + 1) % 3
        j1 = (j + 2) % 3

        if GAME[i0][j] == GAME[i1][j] == target:
            return [i, j]
        if GAME[i][j0] == GAME[i][j1] == target:
            return [i, j]

        if (i == j == 1) or (i == 0 and j == 2) or (i == 2 and j == 0):
            # 2nd diagonal
            gmap = [GAME[0][2], GAME[1][1], GAME[2][0]]
            if gmap[i0] == gmap[i1] == target:
                return [i, j]
        if i == j:
            # main diagonal
            if GAME[i0][i0] == GAME[i1][i1] == target:
                return [i, j]

    return None


def serious_ai(user=COMPUTER):
    '''shenanigans that make computer smart'''
    global GAME
    free = []
    for i in range(3):
        for j in range(3):
            if GAME[i][j] == ' ':
                free.append([i, j])

    opponent = PLAYER
    if opponent == user:
        opponent = COMPUTER

    # win
    pos = find_third_in_row(free, user)
    if pos:
        GAME[pos[0]][pos[1]] = user
        return

    # let PLAYER win't
    pos = find_third_in_row(free, opponent)
    if pos:
        GAME[pos[0]][pos[1]] = user
        return

    #do sth
    [i, j] = free[random.randrange(0, len(free))]
    GAME[i][j] = user


def play_game(gamemode):
    '''starts game in selected gamemode'''
    global GAME
    GAME = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]

    if gamemode != Gamemode.CVC:
        print("Wybierz na klawiaturze numerycznej pole i wciścnij enter. "
              "Liczy się pierwszy wprowadzony znak")
        print("Wprowadź '0' żeby wrócić do menu")

    display()
    computer_first = gamemode == Gamemode.CVP
    for turn in range(9):
        if not (turn % 2) ^ computer_first:
            if gamemode == Gamemode.CVC:
                time.sleep(1)
                serious_ai(PLAYER)
            else:
                while not make_move(PLAYER):
                    display()
                    print("Niepoprawny ruch! Spróbuj ponownie")

        else:
            if gamemode == Gamemode.PVP:
                while not make_move(COMPUTER):
                    display()
                    print("Niepoprawny ruch! Spróbuj ponownie")
            else:
                time.sleep(1)
                serious_ai(COMPUTER)

        display()

        if winner := find_winner():
            print(f"Wygrana należy do {winner}!\n")
            return

    if gamemode == Gamemode.CVC:
        print("GREETINGS PROFESSOR SZANDAŁA\n"
              "\nA STRANGE GAME.\nTHE ONLY WINNING MOVE IS NOT TO PLAY.\n")
    else:
        print("Nikt nie wygrał. Iks De.\n")


def display_modes():
    '''display list of available gamemodes'''
    print("Dostępne tryby:")
    print("[1] PVP")
    print("[2] Gracz kontra Komputer")
    print("[3] Komputer kontra Gracz")
    print("[4] Komputer kontra Komputer")
    print("[0] Wyjście")


def menu():
    '''asks for game mode and if invalid one is provided - lists all options and asks again'''
    while True:
        key = -1
        while True:
            try:
                key = input("Numer trybu gry [1/2/3/4/0]: ")
                if key.lower() == "zero":
                    # as it originally was called
                    key = Gamemode.CVC
                else:
                    key = Gamemode(int(key[0]))
            except ValueError:
                display_modes()
                print("Podano błędny tryb!")
            else:
                break

        if key == Gamemode.EXIT:
            sys.exit()

        play_game(key)


def main():
    '''main method'''  #Yeah, documentation like for Teledyne-Marine's Wayfinder...
    random.seed()

    print("Gra \"Iks i De\" autorstwa B. Hornickiego")
    display_modes()

    menu()


if __name__ == "__main__":
    main()
